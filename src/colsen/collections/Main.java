package colsen.collections;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("-List-");
        List list = new ArrayList();
        list.add("This");
        list.add("is");
        list.add("a");
        list.add("Java");
        list.add("Collection");
        list.add("This");

        for (Object str : list) {
            System.out.println((String)str);
        }

        System.out.println("\n" + "-Queue-");
        Queue queue = new PriorityQueue();
        queue.add("This");
        queue.add("is");
        queue.add("a");
        queue.add("Java");
        queue.add("Collection");
        queue.add("This");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("\n" + "-Set-");
        Set set = new HashSet();
        set.add("This");
        set.add("is");
        set.add("a");
        set.add("Java");
        set.add("Collection");
        set.add("This");

        for (Object str : set) {
            System.out.println((String)str);
        }

        System.out.println("\n" + "-Tree-");
        TreeSet tree = new TreeSet();
        tree.add("This");
        tree.add("is");
        tree.add("a");
        tree.add("Java");
        tree.add("Collection");
        tree.add("This");

        for (Object str : tree) {
            System.out.println((String)str);
        }

        System.out.println("\n" + "-List using Generics-");
        List<Games> gamesList = new LinkedList<Games>();
        gamesList.add(new Games("Call of Duty: Vanguard", "Activision"));
        gamesList.add(new Games("Assassin's Creed Odyssey", "Ubisoft"));
        gamesList.add(new Games("Minecraft", "Mojang"));
        gamesList.add(new Games("BioShock", "2k Games"));
        gamesList.add(new Games("Call of Duty: Cold War", "Activision"));
        gamesList.add(new Games("Assassin's Creed Valhalla", "Ubisoft"));

        for (Games games : gamesList) {
            System.out.println(games);
        }

        Collections.sort(gamesList, new Games.Sortbytitle());
        System.out.println("\nSorted by title using Comparator interface");
        for (int i = 0; i < gamesList.size(); i++)
            System.out.println(gamesList.get(i));

        Collections.sort(gamesList, new Games.Sortbypublisher());
        System.out.println("\nSorted by publisher using Comparator interface");
        for (int i = 0; i < gamesList.size(); i++)
            System.out.println(gamesList.get(i));



    }
}
