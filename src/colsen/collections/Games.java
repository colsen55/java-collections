package colsen.collections;

import java.util.*;
public class Games {

    private String title;
    private String publisher;

    public Games(String title, String publisher) {
        this.title = title;
        this.publisher = publisher;
    }

    public String toString() {
        return "Title: " + title + " Publisher: " + publisher;
    }
    static class Sortbytitle implements Comparator<Games> {
        // Used for sorting in ascending order of title
        public int compare(Games a, Games b)
        {
            return a.title.compareTo(b.title);
        }
    }

    static class Sortbypublisher implements Comparator<Games> {
        // Used for sorting in ascending order of publisher
        public int compare(Games a, Games b)
        {
            return a.publisher.compareTo(b.publisher);
        }
    }
}
